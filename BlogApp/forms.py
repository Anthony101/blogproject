from django import forms
from .models import *
from django.contrib.auth import password_validation
from django.core.exceptions import ValidationError
from captcha.fields import CaptchaField


class UserInfoForm(forms.ModelForm):
    email = forms.EmailField(required=True, label='Aдpec электронной почты')

    class Meta:
        model = BlogUser
        fields = ('username', 'email', 'first_name', 'last_name', 'send_messages')


class UserRegisterForm(forms.ModelForm):
    email = forms.EmailField(required=True, label='Адрес электронной почты')
    password1 = forms.CharField(label='Пароль', widget=forms.PasswordInput,
                                help_text=password_validation.password_validators_help_text_html())
    password2 = forms.CharField(label='Пароль (повторно)', widget=forms.PasswordInput, help_text='Повторите пароль')

    def clean_password1(self):
        password1 = self.cleaned_data['password1']
        if password1:
            password_validation.validate_password(password1)
        return password1

    def clean(self):
        super().clean()
        password1 = self.cleaned_data['password1']
        password2 = self.cleaned_data['password2']
        if password1 and password2 and password1 != password2:
            errors = {'password2': ValidationError('Введенные пароли не совпадают', code='password_mismatch')}
            raise ValidationError(errors)

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data['password1'])
        user.is_active = False
        user.is_activated = False
        if commit:
            user.save()
        user_registrated.send(UserRegisterForm, instance=user)
        return user

    class Meta:
        model = BlogUser
        fields = ('username', 'email', 'password1', 'password2', 'first_name', 'last_name',
                  'send_messages')


class TagForm(forms.ModelForm):

    class Meta:
        model = Tag
        fields = ['title', 'slug']

        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control'}),
            'slug': forms.TextInput(attrs={'class': 'form-control'})
        }

    def clean_slug(self):
        new_slug = self.cleaned_data['slug'].lower()  # здесь можно без get['slug'], т.к. slug точно будет существовать

        if new_slug == 'create':
            raise ValidationError('Слаг с именем "Create" недопустим')

        is_creating = self.instance.pk == None
        if is_creating:
            if Tag.objects.filter(slug__iexact=new_slug).count():
                raise ValidationError('Слаг должен быть уникальным. Слаг с именем "{}" уже существует.'.format(new_slug))
        else:
            if Tag.objects.exclude(id=self.instance.pk).filter(slug__iexact=new_slug).count():
                raise ValidationError('Слаг должен быть уникальным. Слаг с именем "{}" уже существует.'.format(new_slug))
        return new_slug

    def clean_title(self):
        new_title = self.cleaned_data['title'].lower()

        is_creating = self.instance.pk == None
        if is_creating:
            if Tag.objects.filter(title__iexact=new_title).count():
                raise ValidationError('Тег с именем "{}" уже существует.'.format(new_title))
        else:
            if Tag.objects.exclude(id=self.instance.pk).filter(title__iexact=new_title).count():
                raise ValidationError('Тег с именем "{}" уже существует.'.format(new_title))
        return new_title


class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ['title', 'slug', 'content', 'tags']

        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control'}),
            'slug': forms.TextInput(attrs={'class': 'form-control'}),
            'content': forms.Textarea(attrs={'class': 'form-control'}),
            'tags': forms.SelectMultiple(attrs={'class': 'form-control'})
        }

    def clean_slug(self):
        new_slug = self.cleaned_data['slug'].lower()  # здесь можно без get['slug'], т.к. slug точно будет существовать

        if new_slug == 'create':
            raise ValidationError('Слаг с именем "Create" недопустим')

        is_creating = self.instance.pk == None
        if is_creating:
            if Post.objects.filter(slug__iexact=new_slug).count():
                raise ValidationError('Слаг должен быть уникальным. Слаг с именем "{}" уже существует.'.format(new_slug))
        else:
            if Post.objects.exclude(id=self.instance.pk).filter(slug__iexact=new_slug).count():
                raise ValidationError('Слаг должен быть уникальным. Слаг с именем "{}" уже существует.'.format(new_slug))
        return new_slug


class CommentForm(forms.ModelForm):
    captcha = CaptchaField(label='Bвeдитe текст с картинки')

    class Meta:
        model = Comment
        fields = ['text_comment']

        widgets = {
            'text_comment': forms.Textarea(attrs={'class': 'form-control', 'rows': 4})
        }

        labels = {
            'text_comment': 'Ваш комментарий:'
        }
