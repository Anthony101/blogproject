from django.db import models
from django.shortcuts import reverse
from django.utils.text import slugify
from time import time
from django.contrib.auth.models import AbstractUser, User
from django.dispatch import Signal
from .utils import send_activation_notification


def gen_slug(s):
    """ Генератор слагов. """
    new_slug = slugify(s, allow_unicode=True)
    return new_slug + '-' + str(int(time()))


class BlogUser(AbstractUser):
    is_activated = models.BooleanField(default=True, db_index=True, verbose_name='Активирован?')
    send_messages = models.BooleanField(default=True, verbose_name='Направлять оповещения о новых постах?') 

    class Meta(AbstractUser.Meta):
        pass


user_registrated = Signal(providing_args=['instance'])


def user_registrated_dispatcher(sender, **kwargs):
    send_activation_notification(kwargs['instance'])


user_registrated.connect(user_registrated_dispatcher)


class Post(models.Model):
    title = models.CharField(max_length=150, db_index=True, verbose_name="Заголовок")
    slug = models.SlugField(max_length=150, blank=True, unique=True, verbose_name="Слаг")
    content = models.TextField(max_length=1000, db_index=True, verbose_name="Содержание")
    # user = models.ForeignKey(User, on_delete=models.CASCADE)
    tags = models.ManyToManyField('Tag', blank=True, related_name='posts', verbose_name="Теги")
    creation_date = models.DateTimeField(auto_now_add=True)
    like = models.IntegerField(default=0)
    dislike = models.IntegerField(default=0)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('post_details_url', kwargs={'slug': self.slug})

    def get_update_url(self):
        return reverse('post_update_url', kwargs={'slug': self.slug})

    def get_delete_url(self):
        return reverse('post_delete_url', kwargs={'slug': self.slug})

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = gen_slug(self.title)
        super().save(*args, **kwargs)

    class Meta:
        ordering = ['-creation_date']


class Tag(models.Model):
    title = models.CharField(max_length=50, unique=True, verbose_name="Название")
    slug = models.CharField(max_length=50, unique=True, verbose_name="Слаг")

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('tag_details_url', kwargs={'slug': self.slug})

    def get_update_url(self):
        return reverse('tag_update_url', kwargs={'slug': self.slug})

    def get_delete_url(self):
        return reverse('tag_delete_url', kwargs={'slug': self.slug})

    class Meta:
        ordering = ['title']


class Comment(models.Model):
    text_comment = models.TextField(max_length=500)
    user = models.ForeignKey(BlogUser, on_delete=models.CASCADE, related_name='comments')
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='comments')
    creation_date = models.DateTimeField(auto_now_add=True)

    # def __str__(self):
    #     return self.id

    class Meta:
        ordering = ['-creation_date']