from django.urls import path
from .views import *


urlpatterns = [
    path('', posts_list, name='posts_list_url'),
    path('vk_posts/', vk_posts, name='vk_posts_url'),
    path('user/register/activate/<str:sign>/', blog_user_activate, name='user_activate_url'),
    path('user/register/done', BlogUserRegisterDone.as_view(), name='register_done_url'),
    path('user/register/', BlogUserRegister.as_view(), name='user_register_url'),
    path('user/profile/', profile, name='profile_url'),
    path('user/profile/delete/', BlogUserDelete.as_view(), name='profile_delete_url'),
    path('user/profile/update/', BlogUserUpdate.as_view(), name='profile_update_url'),
    path('user/password/change/', BlogUserPasswordChange.as_view(), name='password_change_url'),
    path('login/', BlogUserLogin.as_view(), name='user_login_url'),
    path('logout/', BlogUserLogout.as_view(), name='user_logout_url'),
    path('post/create/', PostCreate.as_view(), name='post_create_url'),
    path('post/<str:slug>/', PostDetails.as_view(), name='post_details_url'),
    path('post/<str:slug>/update/', PostUpdate.as_view(), name='post_update_url'),
    path('post/<str:slug>/delete/', PostDelete.as_view(), name='post_delete_url'),
    path('post/add_comment/<str:slug>/', add_comment, name='add_comment_url'),
    path('post/add_like/<str:slug>/', add_like, name='add_like_url'),
    path('post/add_dislike/<str:slug>/', add_dislike, name='add_dislike_url'),
    path('tags/', tags_list, name='tags_list_url'),
    path('tag/create/', TagCreate.as_view(), name='tag_create_url'),
    path('tag/<str:slug>/', TagDetails.as_view(), name='tag_details_url'),
    path('tag/<str:slug>/update/', TagUpdate.as_view(), name='tag_update_url'),
    path('tag/<str:slug>/delete/', TagDelete.as_view(), name='tag_delete_url'),
]
