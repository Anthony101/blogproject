from django.shortcuts import render, redirect
from django.urls import reverse
from django.shortcuts import get_object_or_404
from django.core.paginator import Paginator
from django.template.loader import render_to_string
from django.core.signing import Signer
from BlogProject.settings import ALLOWED_HOSTS

signer = Signer()


def send_activation_notification(user):
    if ALLOWED_HOSTS:
        host = 'http://' + ALLOWED_HOSTS[0]
    else:
        host = 'http://localhost:8000'
    context = {'user': user, 'host': host, 'sign': signer.sign(user.username)}
    subject = render_to_string('email/activation_letter_subject.txt', context)
    body_text = render_to_string('email/activation_letter_body.txt', context)
    user.email_user(subject, body_text)


def paginate(request, obj_list, page_size, search_query):

    paginator = Paginator(obj_list, page_size)
    page_number = request.GET.get('page', 1)
    page = paginator.get_page(page_number)

    is_paginated = page.has_other_pages()

    if page.has_previous():
        prev_page_url = '?page={}&search={}'.format(page.previous_page_number(), search_query)
    else:
        prev_page_url = ''

    if page.has_next():
        next_page_url = '?page={}&search={}'.format(page.next_page_number(), search_query)
    else:
        next_page_url = ''

    pagination_context = {
        'page_object': page,
        'is_paginated': is_paginated,
        'next_page_url': next_page_url,
        'prev_page_url': prev_page_url,
        'search': search_query
    }
    return pagination_context


class ObjectDetailsMixin:
    model = None
    template = None
    context = {}

    def get(self, request, slug, **kwargs):
        obj = get_object_or_404(self.model, slug__iexact=slug)
        # form = self.model_form()
        context = {
            self.model.__name__.lower(): obj,
            'admin_object': obj,
            'details': True,
            **kwargs
        }
        context.update(self.context)
        return render(request, self.template, context)


class ObjectCreateMixin:
    model_form = None
    template = None

    def get(self, request):
        form = self.model_form()
        return render(request, self.template, {'form': form})

    def post(self, request):
        bound_form = self.model_form(request.POST)

        if bound_form.is_valid():
            new_obj = bound_form.save()
            return redirect(new_obj)
        return render(request, self.template, {'form': bound_form})


class ObjectUpdateMixin:
    model = None
    model_form = None
    template = None

    def get(self, request, slug):
        obj = self.model.objects.get(slug__iexact=slug)  # можно get_object_or_404(), но тег уже должен существовать
        bound_form = self.model_form(instance=obj)
        return render(request, self.template, {'form': bound_form, self.model.__name__.lower(): obj})

    def post(self, request, slug):
        obj = self.model.objects.get(slug__iexact=slug)
        bound_form = self.model_form(request.POST, instance=obj)

        if bound_form.is_valid():
            new_obj = bound_form.save()
            return redirect(new_obj)
        return render(request, self.template, {'form': bound_form, self.model.__name__.lower(): obj})


class ObjectDeleteMixin:
    model = None
    template = None
    redirect_url = None

    def get(self, request, slug):
        obj = self.model.objects.get(slug__iexact=slug)
        return render(request, self.template, {self.model.__name__.lower(): obj})

    def post(self, request, slug):
        obj = self.model.objects.get(slug__iexact=slug)
        obj.delete()
        return redirect(reverse(self.redirect_url))
