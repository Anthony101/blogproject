from django.http import HttpResponse, HttpResponseRedirect
#from .models import BlogUser  #Post, User, Tag
from datetime import datetime, timedelta, date
from django.views.generic import View
from django.views.generic.base import TemplateView
from django.views.generic.edit import UpdateView, CreateView, DeleteView
from .forms import *
from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from .utils import *
from django.urls import reverse, reverse_lazy
from django.contrib import auth
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.core.paginator import Paginator
from django.core.signing import BadSignature
from django.db.models import Q
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import LoginView, LogoutView, PasswordChangeView
from django.contrib.auth import logout
from django.contrib import messages
import requests
import json
import copy
from django.views.decorators.cache import cache_page


@cache_page(180)
def vk_posts(request):
    sak = '94bcca8c94bcca8c94bcca8c8694d7e979994bc94bcca8cc9b8d2ca27cac7a2f308890b'
    group_ids = '143503361, 149218373, 42565717'
    # https://api.vk.com/method/wall.get?owner_id=-149218373&count=3&v=5.100&access_token=94bcca8c94bcca8c94bcca8c8694d7e979994bc94bcca8cc9b8d2ca27cac7a2f308890b

    t1 = datetime.now()
    # получаем информацию о сообществах в виде списка словарей
    try:
        groups = requests.get('https://api.vk.com/method/groups.getById',
                              params={'group_ids': group_ids, 'v': '5.100', 'fields': 'description',
                                      'access_token': sak})
    except:
        return HttpResponse('<h2>Ошибка подключения. Ресурс недоступен или нет подключения к сети интернет.<h2>')
    groups = groups.json()['response']

    """ В каждой группе производится выборка всех постов (записей на стене) в 2019 году. 
        На каждой итерации while выбирается 100 постов группы и список постов дополняется текущей выборкой.
        Список постов считается сформированным (while заврешается), если дата последнего поста 
        текущей выборки ранее 01.01.2019г. При этом, возникающая погрешность в количестве постов последней выборки 
        (с датой ранее 01.01.2019г.) не принимается во внимание: то есть, данные посты также участвуют в анализе.
        В сформированном списке постов отбираются два лидера: по количеству лайков и по количеству репостов.
        В словарях постов-лидеров дата создания приводится к читаемому формату и добавляется ссылка на пост.
        В группу добавляется список постов-лидеров, который доступен в группе по ключу "best_posts".
        Если по лайкам и репостам лидер один, то добавляется только один пост.
    """
    for group in groups:
        group.update({'link': 'https://vk.com/' + group['screen_name']})

        all_posts = []
        offset = 0
        while True:
            r = requests.get('https://api.vk.com/method/wall.get',
                             params={'owner_id': '-' + str(group['id']), 'count': 100, 'offset': offset, 'v': '5.100',
                                     'access_token': sak})
            posts = r.json()['response']['items']
            all_posts.extend(posts)
            post_date_latest = posts[-1]['date']
            min_date_unix = int(datetime(2019, 1, 1).timestamp())
            #min_date_unix = min_date.replace(tzinfo=timezone.utc).timestamp()
            if post_date_latest < min_date_unix: #1546380198:1559410180
                break
            offset += 100

        all_posts.sort(key=lambda dict: dict['likes']['count'])
        # all_posts.reverse()
        best_posts_likes = copy.deepcopy(all_posts[-1])
        best_posts_likes['date'] = datetime.fromtimestamp(best_posts_likes['date']).strftime('%Y-%m-%d %H:%M')
        best_posts_likes.update({'link': 'https://vk.com/wall-' + str(group['id']) + '_' + str(best_posts_likes['id'])})

        all_posts.sort(key=lambda dict: dict['reposts']['count'])
        # all_posts.reverse()
        best_posts_reposts = copy.deepcopy(all_posts[-1])
        best_posts_reposts['date'] = datetime.fromtimestamp(best_posts_reposts['date']).strftime('%Y-%m-%d %H:%M')
        best_posts_reposts.update({'link': 'https://vk.com/wall-' + str(group['id']) + '_' + str(best_posts_reposts['id'])})

        all_posts.sort(key=lambda dict: dict['views']['count'])
        # all_posts.reverse()
        best_posts_views = copy.deepcopy(all_posts[-1])
        best_posts_views['date'] = datetime.fromtimestamp(best_posts_views['date']).strftime('%Y-%m-%d %H:%M')
        best_posts_views.update({'link': 'https://vk.com/wall-' + str(group['id']) + '_' + str(best_posts_views['id'])})

        best_posts = [best_posts_likes, best_posts_reposts, best_posts_views]  # исключаем одинаковые посты, если такие есть
        # for bp in best_posts:
        #     if best_posts.count(bp) > 1:
        #         best_posts.remove(bp)

        group.update({'best_posts': best_posts})

    with open('posts_json', 'w', encoding='utf8') as file:
        json.dump(all_posts, file, indent=2, ensure_ascii=False)

        # if best_posts_likes != best_posts_reposts:
        #     group.update({'best_posts': [best_posts_likes, best_posts_reposts]})
        # else:
        #     group.update({'best_posts': [best_posts_likes]})


    # likes_list = []
    # for post in all_posts:
    #     if post['likes']['count'] > 0:
    #         max = post['likes']['count']
    #         likes_list.append(max)
    # likes_list.sort()
    # likes_level_pop = likes_list[-3]
    #
    # data_posts = []
    # for post in all_posts:
    #     if post['likes']['count'] >= likes_level_pop:
    #         data_post = {
    #             'post_id': post['id'],
    #             'date': post['date'],
    #             'text': post['text'],
    #             'likes': post['likes']['count'],
    #             'reposts': post['reposts']['count']
    #         }
    #         data_posts.append(data_post)
    #
    # data_posts.sort(key=lambda dict: dict['likes'])
    # data_posts.reverse()
    t2 = datetime.now()
    dt = t2 - t1
    return render(request, 'BlogApp/vk_posts.html', context={'groups': groups, 'dtime': dt.seconds, 't1': t1})


class BlogUserRegister(CreateView):
    model = BlogUser
    template_name = 'BlogApp/user_register.html'
    form_class = UserRegisterForm
    success_url = reverse_lazy('register_done_url')


class BlogUserRegisterDone(TemplateView):
    template_name = 'BlogApp/register_done.html'
    # user = BlogUser.objects.get(id=9)
    # send_activation_notification(user)


def blog_user_activate(request, sign):
    try:
        username = signer.unsign(sign)
    except BadSignature:
        return render(request, 'BlogApp/bad_signature.html')
    user = get_object_or_404(BlogUser, username=username)
    if user.is_activated:
        template = 'BlogApp/user_is_activated.html'
    else:
        template = 'BlogApp/activation_done.html'
        user.is_active = True
        user.is_activated = True
        user.save()
    return render(request, template)


class BlogUserUpdate(SuccessMessageMixin, LoginRequiredMixin, UpdateView):
    model = BlogUser
    template_name = 'BlogApp/profile_update.html'
    form_class = UserInfoForm
    success_url = reverse_lazy('profile_url')
    success_message = 'Личные данные пользователя изменены.'
    
    def dispatch(self, request, *args, **kwargs):
        self.user_id = request.user.pk
        return super().dispatch(request, *args, **kwargs)
    
    def get_object(self, queryset=None):
        if not queryset:
            queryset = self.get_queryset()
        return get_object_or_404(queryset, pk=self.user_id)


class BlogUserDelete(LoginRequiredMixin, DeleteView):
    model = BlogUser
    template_name = 'BlogApp/profile_delete.html'
    success_url = reverse_lazy('posts_list_url')
    
    def dispatch(self, request, *args, **kwargs):
        self.user_id = request.user.pk
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        logout(request)
        messages.add_message(request, messages.SUCCESS, 'Пользователь удалён')
        return super().post(request, *args, **kwargs)
    
    def get_object(self, queryset=None):
        if not queryset:
            queryset = self.get_queryset()
        return get_object_or_404(queryset, pk=self.user_id)


class BlogUserLogin(LoginView):
    template_name = 'BlogApp/user_login.html'
    #form = BlogUserForm


class BlogUserLogout(LoginRequiredMixin, LogoutView):
    #template_name = 'BlogApp/user_logout.html'
    pass


class BlogUserPasswordChange(SuccessMessageMixin, LoginRequiredMixin, PasswordChangeView):
    template_name = 'BlogApp/password_change.html'
    success_url = reverse_lazy('profile_url')
    success_message = 'Пароль пользователя изменен.'


@login_required
def profile(request):
    return render(request, 'BlogApp/profile.html')


def posts_list(request):
    search_query = request.GET.get('search', '')

    if search_query:
        posts = Post.objects.filter(Q(title__icontains=search_query) | Q(content__icontains=search_query))
    else:
        posts = Post.objects.all()

    page_size = 3
    context = paginate(request, posts, page_size, search_query)

    return render(request, 'BlogApp/index.html', context)


@login_required()
def add_like(request, slug):
    post = get_object_or_404(Post, slug__iexact=slug)
    post.like += 1
    post.save()
    return redirect(post)


@login_required()
def add_dislike(request, slug):
    post = get_object_or_404(Post, slug__iexact=slug)
    post.dislike += 1
    post.save()
    return redirect(post)


def add_comment(request, slug):
    # if auth.get_user(request).is_authenticated():
    post = get_object_or_404(Post, slug__iexact=slug)
    add_comment_error = None
    if request.POST:
        form = CommentForm(request.POST)
        if request.user.is_authenticated:
            time_limit = timedelta(seconds=60)
            last_comment = Comment.objects.filter(
                user=request.user,
                post=post,
                creation_date__gte=datetime.now()-time_limit)
            if not last_comment.count():
                if form.is_valid():
                    comment = form.save(commit=False)
                    comment.post = post
                    comment.user = request.user
                    comment.save()
                    form = CommentForm()
                    messages.add_message(request, messages.SUCCESS, 'Комментарий добавлен')
                #else:
            else:
                remaining_time = (last_comment.latest('creation_date').creation_date + time_limit) - datetime.now()
                add_comment_error = 'Следующий комментарий можно оставить через {} сек.'.format(remaining_time.seconds)
        else:
            add_comment_error = 'Для добавления комментария требуется авторизация'
        messages.add_message(request, messages.ERROR, add_comment_error)
        context = {
            'post': post,
            'admin_object': post,
            'details': True,
            'form': form
        }
        return render(request, 'BlogApp/post_details.html', context)
    return redirect(post)


class PostDetails(ObjectDetailsMixin, View):
    model = Post
    template = 'BlogApp/post_details.html'
    model_form = CommentForm()
    context = {'form': model_form}


class PostCreate(LoginRequiredMixin, ObjectCreateMixin, View):
    model_form = PostForm
    template = 'BlogApp/post_create.html'
    raise_exception = True


class PostUpdate(LoginRequiredMixin, ObjectUpdateMixin, View):
    model = Post
    model_form = PostForm
    template = 'BlogApp/post_update.html'
    raise_exception = True


class PostDelete(LoginRequiredMixin, ObjectDeleteMixin, View):
    model = Post
    template = 'BlogApp/post_delete.html'
    redirect_url = 'posts_list_url'
    raise_exception = True


def tags_list(request):
    tags = Tag.objects.all()
    return render(request, 'BlogApp/tags_list.html', {'tags': tags})


class TagDetails(ObjectDetailsMixin, View):
    model = Tag
    template = 'BlogApp/tag_details.html'


class TagCreate(LoginRequiredMixin, ObjectCreateMixin, View):
    model_form = TagForm
    template = 'BlogApp/tag_create.html'
    raise_exception = True


class TagUpdate(LoginRequiredMixin, ObjectUpdateMixin, View):
    model = Tag
    model_form = TagForm
    template = 'BlogApp/tag_update.html'
    raise_exception = True


class TagDelete(LoginRequiredMixin, ObjectDeleteMixin, View):
    model = Tag
    template = 'BlogApp/tag_delete.html'
    redirect_url = 'tags_list_url'
    raise_exception = True

